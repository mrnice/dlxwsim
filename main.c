#include <strings.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/file.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <tcl.h>
#include <unistd.h>
#include "dlx.h"
#include <string.h>

static Tcl_Interp *interp;
static DLX *machPtr;

/*
 * Forward references to procedures declared later in this file:
 */

static void Interrupt();


int main (int argc, char *argv[])
{
    if (argc != 4)
    { fprintf (stderr, "usage: dlxwsim <asmfile.h> <startlabel> <insert nops?>\n");
      exit (1);
    }

    interp = Tcl_CreateInterp();

    machPtr = Sim_Create (MEMSIZE, interp,
                NUM_ADD_UNITS, FP_ADD_LATENCY,
                NUM_MUL_UNITS, FP_MUL_LATENCY,
                NUM_DIV_UNITS, FP_DIV_LATENCY,
                strcmp (argv[3], "1") == 0);

    //(void) signal (SIGINT, Interrupt);

    char *p;
    char *loadCmd = malloc (strlen (argv[1]) + 6);
    strcpy (loadCmd, "load ");  strcat (loadCmd, argv[1]);
    Tcl_Eval (interp, loadCmd, 0, &p);
	if (*interp->result != 0)  { puts (interp->result);  return 1; }
    Sym_MemoryDump (machPtr);
    puts ("");
    char *goCmd = malloc (strlen (argv[2]) + 4);
    strcpy (goCmd, "go ");  strcat (goCmd, argv[2]);
    Tcl_Eval (interp, goCmd, 0, &p);
	if (*interp->result != 0)  { puts (interp->result);  return 1; }
    puts ("");
    Sym_MemoryDump (machPtr);
    //Tcl_Eval (interp, "stats all", 0, &p);

/*
    char *arg[] = {"load", "x.s"};
    int ret = Asm_LoadCmd (machPtr, interp, 2, arg);
    if (ret != TCL_OK)
    { fprintf (stderr, "error loading"); exit (1); }

    arg[1] = "main";
    ret = Sim_GoCmd (machPtr, interp, 2, arg);
    if (ret != TCL_OK)
    { fprintf (stderr, "error running"); exit (1); }

    arg[1] = "all";
    ret = Sim_DumpStats (machPtr, interp, 2, arg);
    if (ret != TCL_OK)
    { fprintf (stderr, "error dumping stats"); exit (1); }
*/
    return 0;
}


/*
 *----------------------------------------------------------------------
 *
 * Interrupt --
 *
 *      This procedure is invoked when the interrupt key is typed:
 *      it causes the simulation to stop.
 *
 * Results:
 *      None.
 *
 * Side effects:
 *      Causes simultor to stop after next instruction.
 *
 *----------------------------------------------------------------------
 */

static void
Interrupt()
{
  fprintf( stderr, "interrupt\n" );
  Sim_Stop(machPtr);
}



/*
 *----------------------------------------------------------------------
 *
 * Main_QuitCmd --
 *
 *      This procedure is invoked to process the "quit" Tcl command.
 *      See the user documentation for details on what it does.
 *
 * Results:
 *      None:  this command never returns.
 *
 * Side effects:
 *      The program exits.
 *
 *----------------------------------------------------------------------
 */

/* ARGSUSED */
int
Main_QuitCmd(machPtr, interp, argc, argv)
    DLX *machPtr;                       /* Machine description. */
    Tcl_Interp *interp;                 /* Current interpreter. */
    int argc;                           /* Number of arguments. */
    char **argv;                        /* Argument strings. */
{
    exit(0);
    return TCL_OK;                      /* Never gets executed. */
}
